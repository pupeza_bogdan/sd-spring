package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import jakarta.transaction.Transactional;
import ro.tuc.ds2020.entities.Role;
import ro.tuc.ds2020.entities.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

        Optional<User> findByEmail(String email);

        List<User> findByName(String name);

        List<User> findByRole(Role role);

        List<User> findAll();

        @Modifying
        @Transactional
        @Query(value = "update User u " +
                        "set u.name = :name, " +
                        "u.role = :role " +
                        "where u.id = :id ")
        int updateUser(
                        @Param(value = "id") UUID id,
                        @Param(value = "name") String name,
                        @Param(value = "role") Role role);

        @Modifying
        @Transactional
        @Query(value = "update User u " +
                        "set u.name = :name " +
                        "where u.id = :id ")
        int updateName(
                        @Param(value = "id") UUID id,
                        @Param(value = "name") String name);

        @Modifying
        @Transactional
        @Query(value = "update User u " +
                        "set u.role = :role " +
                        "where u.id = :id ")
        int updateRole(
                        @Param(value = "id") UUID id,
                        @Param(value = "role") Role role);
}
