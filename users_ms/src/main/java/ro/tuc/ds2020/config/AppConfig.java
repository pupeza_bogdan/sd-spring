package ro.tuc.ds2020.config;

public class AppConfig {
    public static boolean isDockerApp = true;
    public static String monitoringHost = isDockerApp ? "monitoring-ms": "localhost";
    public static String devicesHost = isDockerApp ? "devices-ms": "localhost";

}
