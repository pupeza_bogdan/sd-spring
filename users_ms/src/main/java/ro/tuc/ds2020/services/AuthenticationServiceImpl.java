package ro.tuc.ds2020.services;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import ro.tuc.ds2020.dao.request.SignUpRequest;
import ro.tuc.ds2020.dao.request.SigninRequest;
import ro.tuc.ds2020.dao.response.JwtAuthenticationResponse;
import ro.tuc.ds2020.dtos.builders.UserBuilder;
import ro.tuc.ds2020.entities.Role;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.mappers.DevicesMapper;
import ro.tuc.ds2020.my_logger.MyLoggeer;
import ro.tuc.ds2020.repositories.UserRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final DevicesMapper devicesMapper;

    @Override
    public JwtAuthenticationResponse signup(SignUpRequest request) {
        var user = User.builder()
                .name(request.getName())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.ROLE_USER).build();
        userRepository.save(user);
        devicesMapper.insertUser(UserBuilder.toDevicesUserDTO(user));
        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse.builder().token(jwt).build();
    }

    @Override
    public JwtAuthenticationResponse signin(SigninRequest request) {
        MyLoggeer.print(request.toString());
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
            User user = userRepository.findByEmail(request.getEmail())
                    .orElseThrow(() -> new IllegalArgumentException("Invalid email or password."));
            MyLoggeer.print(String.valueOf(user));
            var jwt = jwtService.generateToken(user);
            return JwtAuthenticationResponse.builder()
                    .token(jwt)
                    .userId(user.getId())
                    .userRole(user.getRole())
                    .build();
        }catch (Exception exception){
            MyLoggeer.print(exception.toString());
            throw  exception;
        }
    }
}
