package ro.tuc.ds2020.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.mappers.DevicesMapper;
import ro.tuc.ds2020.dtos.builders.UserBuilder;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.mappers.MonitoringMapper;
import ro.tuc.ds2020.my_logger.MyLoggeer;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final DevicesMapper devicesMapper;
    private final MonitoringMapper monitoringMapper;


    public UserDetailsService userDetailsService() {
        return username -> userRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    public List<UserDTO> findUsers() {
        List<User> userList = userRepository.findAll();
        return userList.stream()
                .map(UserBuilder::toUserDTO)
                .collect(Collectors.toList());
    }

    public UserDTO findUserById(UUID id) {
        Optional<User> prosumerOptional = userRepository.findById(id);
        if (prosumerOptional.isEmpty()) {
            throw new ResourceNotFoundException(User.class.getSimpleName());
        }
        return UserBuilder.toUserDTO(prosumerOptional.get());
    }

    public UUID insert(UserDTO userDTO, String password) {
        User user = User.builder()
                .name(userDTO.getName())
                .email(userDTO.getEmail())
                .password(password)
                .role(userDTO.getRole()).build();
        user = userRepository.save(user);
        MyLoggeer.print(user + " was inserted in db");
        var response1 = devicesMapper.insertUser(UserBuilder.toDevicesUserDTO(user));
        var response2 = monitoringMapper.insertUser(UserBuilder.toDevicesUserDTO(user));
        MyLoggeer.print("Response: " + response1 + " / " + response2);
        return user.getId();
    }

    public UserDTO update(UUID userId, UserDTO userDTO) {
        User user = UserBuilder.toEntity(userDTO);
        int rowsUpdated = 0;
        if (userDTO.getRole() != null && userDTO.getName() != null) {
            rowsUpdated = userRepository.updateUser(userId, user.getName(),
                    user.getRole());
        } else if (userDTO.getRole() == null && userDTO.getName() != null) {
            rowsUpdated = userRepository.updateName(userId, user.getName());
        } else if (userDTO.getRole() != null) {
            rowsUpdated = userRepository.updateRole(userId, user.getRole());
        } else {
            MyLoggeer.print("Nothing to update...");
        }
        MyLoggeer.print("Rows updated: " + rowsUpdated);
        return findUserById(userId);
    }

    public void deleteById(UUID id) {
        boolean result1 = devicesMapper.deleteUserById(id);
        boolean result2 = monitoringMapper.deleteUserById(id);
        if(result1 && result2) {
            userRepository.deleteById(id);
        }
    }
}
