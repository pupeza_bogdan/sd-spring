package ro.tuc.ds2020.dtos;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.tuc.ds2020.entities.Role;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DevicesUserDTO {
    private UUID id;
    private String password;
    private String email;
    private Role role;
}
