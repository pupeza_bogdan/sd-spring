package ro.tuc.ds2020.mappers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ro.tuc.ds2020.config.AppConfig;
import ro.tuc.ds2020.services.JwtService;

@Service
public class DevicesMapper extends UserMapper {

    private static String usersUrl() {
        return "http://" + AppConfig.devicesHost + ":8081/api/v1/users";
    }
    public DevicesMapper(RestTemplate restTemplate, JwtService jwtService){
        super(usersUrl(), restTemplate, jwtService);
    }

}
