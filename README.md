# DS Project

## TODOS

- CRUD for Users
- CRUD for Devices
- If we insert a device with a userId, we should check if it exists
- Update FE
- Use docker-compose

## RestTemplate 
### Users => Devices
- [WORKS] add user in Users => add user in Devices 
- [WORKS] signup user in Users => add user in Devices
- [DONE] TODO: save the password, remove name
- [DONE] delete user in Users => delete user in Devices + delete all devices assigned to the user
- update user in Users => update user in Devices

### Devices => Users
- add device in Devices => check userId (from Devices or from Users?)


## Setup +  Requirements
* mvn 3.9.5
* java 17.0.6
* node 16.20.2
* npm 8.19.4
* mysql
* Intellij + VSCode
* Docker
* back-end init: add configuration in IntelliJ at ```ro.tuc.ds2020.Ds2020Application```
* back-end start: click on run button (Shift + F10) 
* front-end init: ```npm install```
* front-end start: ```npm start```

## DOCKER set up
### MySql Server
* ```docker run -it --name mysqldb-users --network springboot-mysql-net -e MYSQL_ROOT_PASSWORD=1234 -e MYSQL_DATABASE=users-db -e MYSQL_USER=sys -e MYSQL_PASSWORD=1234 -d mysql:5.7```
* ```docker run -it --name mysqldb-devices --network springboot-mysql-net -e MYSQL_ROOT_PASSWORD=1234 -e MYSQL_DATABASE=devices-db -e MYSQL_USER=sys -e MYSQL_PASSWORD=1234 -d mysql:5.7```

### Users Microservice
* ```docker build -t users_ms_image .```
* ```docker run --network springboot-mysql-net --name users_ms -p 8080:8080 users_ms_image_2```

### Devices Microservice
* ```docker build -t devices_ms_image .```
* ```docker run --network springboot-mysql-net --name devices_ms -p 8081:8081 devices_ms_image```


### Front End
* ```docker build -t  fe-image  . ```
* ```docker-compose up -d ```


## MySQL commands
* ```mysql -u sys/root -p ```
* password: ```1234```
* ```use users-db```
* ```select * from user```
* ```update user u set u.role="ROLE_ADMIN" where u.email="bp@admin.com"```

* ```use devices-db```
* ```select * from user```
* ```update user u set u.role="ROLE_ADMIN" where u.email="bp@admin.com"```

## Endpoints
### Auth
- POST localhost:8080/api/v1/auth/signup + body
- POST localhost:8080/api/v1/auth/signin + body
### Users 
- GET localhost:8080/api/v1/users // all users
- GET localhost:8080/api/v1/users/{{user_id}} // user with id user_id
- POST localhost:8080/api/v1/users + body // creates user with data from body
- PATCH localhost:8080/api/v1/users/{{user_id}} + body: JSON // updates user with id user_id 
- DELETE localhost:8080/api/v1/users/{{user_id}} // deletes user with id user_id 
- GET localhost:8080/api/v1/users/{{user_id}}/devices // devices of the user with id user_id
### Devices
- GET localhost:8081/api/v1/devices // all users
- GET localhost:8081/api/v1/devices/{{device_id}} // device with id device_id
- POST localhost:8081/api/v1/devices + body // creates device with data from body
- PATCH localhost:8081/api/v1/devices/{{device_id}} + body: JSON // updates device with id device_id
- DELETE localhost:8081/api/v1/devices/{{device_id}} // deletes device with id device_id

## Conceptual Architecture

![](doc/conceptual_architecture.png)

## Deployment Diagram

![](doc/deployment_diagram.png)

## CORS config for Spring Security

- Add `@CrossOrigin(origins = {"http://localhost", "http://localhost:3000"})` at the beginning of the `@Controller` class
- Add the `@Controller`'s mapping path to the filter chain in `SecurityConfiguration`

## Learning
### DTO
DTO (Data Transfer Object) transfer only the information required. 
It is an object that is used to encapsulate data,
and send it from one subsystem of an application to another.  
  
DTO is an abbreviation for Data Transfer Object, so it is used to transfer the data between classes 
and modules of your application.
* DTO should only contain private fields for your data, getters, setters, and constructors.
* DTO is not recommended to add business logic methods to such classes, but it is OK to add some util methods.

### DAO
DAO (Data Access Object) is a pattern that provides an abstract interface to some 
type of database or other persistence mechanism. By mapping application calls to the 
persistence layer, the DAO provides data operations without exposing database details.  
  
DAO is an abbreviation for Data Access Object, so it should encapsulate the logic for retrieving, 
saving and updating data in your data storage (a database, a file-system, whatever).
