package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.entities.Device;

import java.util.Map;
import java.util.UUID;

public class DeviceBuilder {

    private DeviceBuilder() {
    }

    public static DeviceDTO toDeviceDTO(Device device) {
        return DeviceDTO.builder()
                .id(device.getId())
                .userId(device.getUserId())
                .description(device.getDescription())
                .mhec(device.getMhec())
                .build();
    }

    public static Device entityFromMap(Map<String, String> map){
        return Device.builder()
                .id(UUID.fromString(map.get("id")))
                .userId(UUID.fromString(map.get("user_id")))
                .description(map.get("description"))
                .address(map.get("address"))
                .mhec(Integer.parseInt(map.get("mhec")))
                .build();
    }
}
