package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import jakarta.transaction.Transactional;
import ro.tuc.ds2020.entities.Device;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DeviceRepository extends JpaRepository<Device, UUID> {

        List<Device> findByUserId(UUID userId);
        @Modifying
        @Transactional
        @Query(value = "update Device d set " +
                        "d.userId = :userId, " +
                        "d.description = :description, " +
                        "d.address = :address, " +
                        "d.mhec = :mhec " +
                        "where d.id = :id ")
        int updateDevice(
                        @Param(value = "id") UUID id,
                        @Param(value = "userId") UUID userId,
                        @Param(value = "description") String description,
                        @Param(value = "address") String address,
                        @Param(value = "mhec") int mhec);
}
