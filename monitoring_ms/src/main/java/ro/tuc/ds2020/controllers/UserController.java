package ro.tuc.ds2020.controllers;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.my_logger.MyLoggeer;
import ro.tuc.ds2020.services.UserService;
import ro.tuc.ds2020.services.DeviceService;

import javax.validation.Valid;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/v1/users")
@CrossOrigin(origins = {"http://localhost", "http://localhost:3000"})
public class UserController {

    private final UserService userService;
    private final DeviceService deviceService;

    UserController(UserService userService, DeviceService deviceService) {
        this.userService = userService;
        this.deviceService = deviceService;
    }

    @PostMapping()
    public ResponseEntity<Map<String, UUID>> insertUser(@Valid @RequestBody UserDTO userDTO) {
        MyLoggeer.print("insert User" + userDTO);
        UUID userID = userService.insert(userDTO);
        return new ResponseEntity<>(Map.of("id", userID), HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserDTO> getUser(@PathVariable("id") UUID userId) {
        UserDTO userDTO = userService.findUserById(userId);
        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

    @PatchMapping(value = "/{id}")
    public ResponseEntity<UserDTO> updateUser(@PathVariable("id") UUID userId, @Valid @RequestBody UserDTO userDTO) {
        MyLoggeer.print(userDTO.toString());
        UserDTO user = userService.update(userId, userDTO);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Map<String, String>> deleteUser(@PathVariable("id") UUID userId) {
        userService.deleteById(userId);
        deviceService.deleteByUserId(userId);
        return new ResponseEntity<>(Map.of("status", "success"), HttpStatus.OK);
    }
}
