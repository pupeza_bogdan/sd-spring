package ro.tuc.ds2020.rabbit_mq;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/*
rabbit mq:
    * run docker compose
    * user/password: guest
 */

// Simulator class
public class Send {
    public final static String QUEUE_NAME = "monitoringQueue";
    public final static String TOPIC_EXCHANGE_NAME = "monitoringTopic";
    public final static boolean isTopic = true;

    public static double readFromCSV(BufferedReader br){
        double result = -1.0;
        try {
            String value = br.readLine();
            if(value == null){
                br.close();
                return  result;
            }
            return Double.parseDouble(value);
        } catch(IOException ioe) {
            System.out.println("Error: " + ioe.getMessage());
        }
        return result;
    }

    public static void main(String[] argv) throws Exception {
        File file = new File("sensor.csv");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String deviceId = argv[0];
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        while(true) {
            double measurementValue = readFromCSV(br);
            if(measurementValue < 0.0){
                System.out.println("Simulation Ended");
                break;
            }
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            Map<String, Object> measurement = Map.of(
                    "timestamp", "" + timestamp.getTime(),
                    "device_id", deviceId,
                    "measurement_value", measurementValue
            );
            byte[] message = measurement.toString().getBytes(StandardCharsets.UTF_8);
            publishMessage(factory, isTopic, message);
            System.out.println(" [x] Sent '" + measurement + "'");

            TimeUnit.SECONDS.sleep(10);
        }
    }

    public static void publishMessage(
            ConnectionFactory factory,
            boolean isTopic,
            byte[] message
    ) {
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            if(isTopic) {
                channel.exchangeDeclare(TOPIC_EXCHANGE_NAME, BuiltinExchangeType.TOPIC, true);
                channel.basicPublish(TOPIC_EXCHANGE_NAME, "", null, message);
            } else {
                 channel.queueDeclare(QUEUE_NAME, true, false, false, null);
                channel.basicPublish( "", QUEUE_NAME, null, message);
            }
        } catch (IOException | TimeoutException e) {
            throw new RuntimeException(e);
        }
    }
}