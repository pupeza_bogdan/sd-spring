package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.DeviceDetailsDTO;
import ro.tuc.ds2020.entities.Device;

public class DeviceBuilder {

    private DeviceBuilder() {
    }

    public static DeviceDTO toDeviceDTO(Device device) {
        return DeviceDTO.builder()
                .id(device.getId())
                .userId(device.getUserId())
                .description(device.getDescription())
                .mhec(device.getMhec())
                .build();
    }

    public static DeviceDetailsDTO toDeviceDetailsDTO(Device device) {
        return DeviceDetailsDTO.builder()
                .id(device.getId())
                .userId(device.getUserId())
                .description(device.getDescription())
                .address(device.getAddress())
                .mhec(device.getMhec())
                .build();
    }

    public static Device toEntity(DeviceDetailsDTO deviceDetailsDTO) {
        return Device.builder()
                .userId(deviceDetailsDTO.getUserId())
                .description(deviceDetailsDTO.getDescription())
                .address(deviceDetailsDTO.getAddress())
                .mhec(deviceDetailsDTO.getMhec())
                .build();
    }
}
