package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviceDetailsDTO {
    private UUID id;
    private UUID userId;
    @NotNull
    private String description;
    @NotNull
    private String address;
    @NotNull
    private int mhec;
}
