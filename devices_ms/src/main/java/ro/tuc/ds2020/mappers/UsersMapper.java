package ro.tuc.ds2020.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ro.tuc.ds2020.my_logger.MyLoggeer;
import ro.tuc.ds2020.services.JwtService;
import ro.tuc.ds2020.services.UserService;

import java.util.UUID;

@Service
public class UsersMapper {
    private final RestTemplate restTemplate;
    private final JwtService jwtService;

    public UsersMapper(RestTemplate restTemplate, JwtService jwtService, UserService userService){
        this.restTemplate = restTemplate;
        this.jwtService = jwtService;
    }

    public HttpHeaders basicHeaders(){
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
    public HttpHeaders authHeaders(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        String jwt = jwtService.generateToken(userDetails);
        var headers = basicHeaders();
        headers.setBearerAuth(jwt);
        return headers;
    }

    public boolean checkUserById(UUID userId){
        try {
            HttpEntity<String> entity = new HttpEntity<>(authHeaders());
            var response = restTemplate.exchange(
                    "http://localhost:8080/api/v1/users/" + userId.toString(),
                    HttpMethod.GET,
                    entity,
                    String.class
            );
            MyLoggeer.print(response.toString());
        } catch (Exception e){
            //TODO: handle exception
            MyLoggeer.print(e.toString());
            return false;
        }
        return true;
    }
}
