package ro.tuc.ds2020.services;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.builders.UserBuilder;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.UserRepository;

import java.util.Optional;
import java.util.UUID;


@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    public UserDetailsService userDetailsService() {
        return username -> userRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    public UserDTO findUserById(UUID id) {
        Optional<User> prosumerOptional = userRepository.findById(id);
        if (prosumerOptional.isEmpty()) {
            throw new ResourceNotFoundException(User.class.getSimpleName());
        }
        return UserBuilder.toUserDTO(prosumerOptional.get());
    }

    public UUID insert(UserDTO userDTO) {
        User user = User.builder()
                .id(userDTO.getId())
                .password(userDTO.getPassword())
                .email(userDTO.getEmail())
                .role(userDTO.getRole()).build();
        user = userRepository.save(user);
        return user.getId();
    }

    public UserDTO update(UUID userId, UserDTO userDTO) {
        User user = UserBuilder.toEntity(userDTO);
        if (userDTO.getRole() != null) {
            userRepository.updateUser(userId, user.getRole());
        }
        return findUserById(userId);
    }

    public void deleteById(UUID id) {
        userRepository.deleteById(id);
    }

}
